import json

from simple_web_framework import App
from database import EWalletDB
from config import db_file


application = App()
db = EWalletDB(db_file)


@application.add_handler(r'^/$')
def index_page_handler(environ, url_args):

    return 200, {}, ''


@application.add_handler(r'^/send/$', methods=['GET', 'POST'])
def send_method_handler(environ, url_args):
    try:
        post_data = json.loads(
            environ['wsgi.input'].read(int(environ.get('CONTENT_LENGTH', 0))).decode()
        )
        new_task_uuid = db.produce_new_task(**post_data)
    except Exception as _err:
        return 501, {}, "Can't parse json data! Check your data format!"
    else:
        return 200, {}, {'id': '%s' % new_task_uuid}


@application.add_handler(r'^/result/(?P<task_id>[a-z0-9\-]+)/$')
def result_method_handler(environ, url_args):
    current_task = db.get_current_task(url_args.get('task_id'))

    return 200, {}, current_task
