## usage
### need python3 and uwsgi module
```
pip install uwsgi
```
### cd project dir and init db
```
python3 init_db.py
```

### run producer process
```
uwsgi --wsgi-file web_app.py --http :8000
```

### run consumer process
```
python3 queue_app.py
```

### producer api
#### Send method (http://127.0.0.1:8000/send/)
Receives a POST request with data in JSON.
Request body has URL parameter.

```
curl -X POST -d '{"user_from": "test1", "user_to": "test3", "cash": "44"}' 'http://127.0.0.1:8000/send/'
```
Returns ID of a created task (JSON).
```
{"id": "2a71a475-d354-4fc8-b170-37a1d08da986"}
```

#### Result method (http://127.0.0.1:8000/result/2a71a475-d354-4fc8-b170-37a1d08da986)
Receives a GET request with ID param.
Returns information about task with specified ID.
```
curl -X GET 'http://127.0.0.1:8000/result/ea942392-8dd3-4f25-81d8-9c53bbd5f847/'
{"status_id": "completed", "user_from": 1, "user_to": 3, "cash": 44.0, "uuid": "ea942392-8dd3-4f25-81d8-9c53bbd5f847"}
```

