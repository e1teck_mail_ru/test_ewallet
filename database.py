import hashlib
import sqlite3
import uuid
from collections import OrderedDict

from helpers import singleton
from logger import CustomLogger


@singleton
class EWalletDB:
    def __init__(self, db_file, db_init=False):
        self.logger = CustomLogger('db_operatons').logger
        self.db = self.init_conn(db_file)
        self.statuses_data = self.set_statuses((
            'new',
            'pending',
            'completed',
            'error'
        ))
        if not db_init:
            self.update_old_tasks()

    @staticmethod
    def set_statuses(statuses):
        return {v: k for k, v in enumerate(statuses, 1)}

    def init_conn(self, db_file):
        try:
            conn = sqlite3.connect(db_file, check_same_thread=False)
        except sqlite3.Error as _err:
            self.logger.error('Init error: {}'.format(_err))
        else:
            return conn

    def create_statuses_table(self):

        cursor = self.db.cursor()
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS statuses (
                id integer PRIMARY KEY,
                status text NOT NULL
            );"""
        )

        try:
            for val, id in self.statuses_data.items():
                cursor.execute("""
                    INSERT INTO statuses ('id', 'status')
                    VALUES
                    ('%d', '%s')""" % (id, val)
                )
            self.db.commit()
        except Exception as _err:
            self.logger.error('Some errors: {}'.format(_err))

    def create_accounts_table(self):
        cursor = self.db.cursor()
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS accounts (
             id integer PRIMARY KEY AUTOINCREMENT,
             user_id integer UNIQUE NOT NULL,
             account real DEFAULT 0,
             FOREIGN KEY (user_id) REFERENCES users (id)
            );"""
        )
        self.db.commit()

    def create_users_table(self):
        cursor = self.db.cursor()
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS users (
             id integer PRIMARY KEY AUTOINCREMENT,
             login text UNIQUE NOT NULL,
             passwd text NOT NULL
            );"""
        )
        self.db.commit()

    def create_tasks_table(self):
        cursor = self.db.cursor()
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS tasks (
             id integer PRIMARY KEY AUTOINCREMENT,
             status_id integer NOT NULL,
             user_from integer NOT NULL,
             user_to integer NOT NULL,
             cash real NOT NULL,
             uuid text UNIQUE NOT NULL,
             FOREIGN KEY (status_id) REFERENCES statuses (id),
             FOREIGN KEY (user_from) REFERENCES users (id),
             FOREIGN KEY (user_to) REFERENCES users (id)
            );"""
        )
        self.db.commit()

    def init_tables(self):
        try:
            self.create_statuses_table()
            self.create_accounts_table()
            self.create_users_table()
            self.create_tasks_table()
        except Exception as _err:
            self.logger.error('Create table error: {}'.format(_err))
        else:
            self.logger.info('Tables was created...')

    def create_user(self, login, passwd):
        try:
            passwd_md5 = hashlib.md5(passwd.encode('utf-8')).hexdigest()
            cursor = self.db.cursor()
            cursor.execute("""
                INSERT INTO users ('login', 'passwd')
                VALUES ('%s', '%s')""" % (login, passwd_md5)
            )
            self.db.commit()
        except Exception as _err:
            self.logger.error('Create user error: {}'. format(_err))

    def create_account(self, user_id, account):
        try:
            cursor = self.db.cursor()
            cursor.execute("""
                INSERT INTO accounts ('user_id', 'account')
                VALUES (%d, %d)""" % (user_id, account)
            )
        except Exception as _err:
            self.logger.error('Create account error: {}'.format(_err))
        else:
            self.db.commit()

    def init_users_accounts(self, users_list):
        for user_item in users_list:
            try:
                account = user_item.pop('account')
                self.create_user(**user_item)
                user_id  = self.get_user_id(user_item['login'])
                if user_id:
                    self.create_account(user_id, account)
            except Exception as _err:
                self.logger.error('Init users and accounts errors: {}'.format(_err))

    def update_old_tasks(self):
        try:
            cursor = self.db
            cursor.execute("""
                UPDATE tasks set status_id = 1
                where status_id = 2;"""
            )
            self.db.commit()
        except Exception as _err:
            self.logger.error('%s\n' % _err)
        else:
            self.logger.info('Old not completed tasks was updated\n')

    def get_user_id(self, login):
        cursor = self.db
        user_data = cursor.execute(
            """
            SELECT id from users where login = '%s'
            """ % (login)
        ).fetchone()

        if not user_data and len(user_data):
            return

        return user_data[0]

    def get_user_account(self, user_id):
        try:
            cursor = self.db.cursor()
            cur_account = cursor.execute("""
                SELECT account from accounts where user_id = %d""" % user_id
            ).fetchone()
        except Exception as _err:
            self.logger.error('Get user account: {}'.format(_err))
        else:
            return cur_account[0] if cur_account and len(cur_account) else None

    def produce_new_task(self, user_from, user_to, cash):
        cash = float(cash)
        user_from_id = self.get_user_id(user_from)
        user_to_id = self.get_user_id(user_to)

        if not all((user_from_id, user_to_id)):
            return

        try:
            cursor = self.db
            _uuid = uuid.uuid4()
            cursor.execute("""
                INSERT INTO tasks ('status_id', 'user_from', 'user_to', 'cash', 'uuid')
                VALUES ('%d', '%d', '%d', '%d', '%s')""" % (1, user_from_id, user_to_id, cash, _uuid)
            )
            self.db.commit()
        except Exception as _err:
            self.logger.error('%s\n' % _err)
            return
        else:
            self.logger.info(
                'Produce new operation from user: {}, to user: {}, cash: {}, uuid = {}\n'.format(
                    user_from, user_to, cash, _uuid
                )
            )
            return _uuid

    def get_current_task(self, uuid):
        current_task_item = None
        try:
            cursor = self.db
            fields = 'status_id', 'user_from', 'user_to', 'cash', 'uuid'
            current_task = cursor.execute(
                """
                SELECT statuses.status, user_from, user_to, cash, uuid
                FROM tasks LEFT JOIN statuses ON tasks.status_id = statuses.id where uuid = '%s'
                """ % uuid
            ).fetchone()

            if current_task:
                current_task_item = OrderedDict(zip(fields, current_task))

        except Exception as _err:
            self.logger.error('%s\n' % _err)
        else:
            return current_task_item

    def change_task_status(self, uuid, new_status):
        try:
            _status = self.statuses_data[new_status]
            cursor = self.db
            cursor.execute("""
                UPDATE tasks set status_id = '%d'
                where uuid = '%s';""" % (_status, uuid)
            )
        except Exception as _err:
            self.logger.error('%s\n' % _err)
            return
        else:
            self.db.commit()
            return new_status

    def get_new_task(self):
        item_dict = None

        try:
            cursor = self.db
            fields = 'status_id', 'user_from', 'user_to', 'cash', 'uuid'
            task_item = cursor.execute("""
                SELECT statuses.status, user_from, user_to, cash, uuid
                FROM tasks LEFT JOIN statuses ON tasks.status_id = statuses.id   
                where statuses.status = 'new' order by tasks.id"""
            ).fetchone()

            if not task_item:
                return

            item_dict = OrderedDict(zip(fields, task_item))

        except Exception as _err:
            self.logger.error('Get new task error: {}'.format(_err))

        return item_dict

    def make_task_transaction(self, item_dict, limit_user_to=None):
        _uuid = item_dict.pop('uuid')
        user_from_account = self.get_user_account(item_dict['user_from'])
        user_to_account = self.get_user_account(item_dict['user_to'])

        if not user_from_account > item_dict['cash']:
            self.change_task_status(_uuid, 'error')
            self.logger.error(
                "Can't transfer ({}) from user_id: {} to user_id {} cash: {}, reason: {}".format(
                    _uuid, item_dict['user_from'], item_dict['user_to'], item_dict['cash'],
                    'Not enouth money: {} > {}'.format(user_from_account, item_dict['cash'])
                )
            )
            return

        if limit_user_to and not user_to_account < limit_user_to:
            self.change_task_status(_uuid, 'error')
            self.logger.error(
                "Can't transfer ({}) from user_id: {} to user_id {} cash: {}, reason: {}".format(
                    _uuid, item_dict['user_from'], item_dict['user_to'], item_dict['cash'],
                    'outer limit {} < {}'.format(user_to_account, limit_user_to)
                )
            )
            return

        self.db.isolation_level = None
        cursor = self.db.cursor()

        try:
            cursor.execute("begin")
            cursor.execute(
                "update accounts set account = account - %d where user_id = %d" % (
                    item_dict['cash'], item_dict['user_from']
                )
            )
            cursor.execute(
                "update accounts set account = account + %d where user_id = %d" % (
                    item_dict['cash'], item_dict['user_to']
                )
            )
            cursor.execute(
                "update tasks set status_id = %s where uuid = '%s'" % (self.statuses_data['completed'], _uuid)
            )
            cursor.execute("commit")
        except self.db.Error as _err:
            self.logger.error('Some errors: {}'.format(_err))
            cursor.execute('rollback')

        else:
            self.logger.info('Transaction with uuid: {} is completed'.format(_uuid))

