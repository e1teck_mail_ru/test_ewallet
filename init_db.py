from config import db_file
from database import EWalletDB


db = EWalletDB(db_file, db_init=True)
db.init_tables()
users_list = [
    {'login': 'test1', 'passwd': 'tt123', 'account': 50},
    {'login': 'test2', 'passwd': 'tt124', 'account': 70},
    {'login': 'test3', 'passwd': 'tt125', 'account': 55}
]
db.init_users_accounts(users_list)
