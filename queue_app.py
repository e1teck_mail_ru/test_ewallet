import queue
import threading
import time

from database import EWalletDB
from config import db_file, transfer_user_limit


class QueueTaskConsumer:

    def __init__(self, db_file):
        self.main_queue = queue.Queue()
        self.thread_number = 1
        self.thread_list = []
        self.db_inst = EWalletDB(db_file)

    def thread_func(self, name):
        while True:
            item = self.main_queue.get()
            self.db_inst.make_task_transaction(item, transfer_user_limit)
            self.main_queue.task_done()

    def start_threads(self):
        for thread_iter in range(self.thread_number):
            _thread = threading.Thread(
                target=self.thread_func,
                name='Thread-{}'.format(thread_iter),
                args=('Thread-{}'.format(thread_iter),),
                daemon=True
            )

            self.thread_list.append(_thread)
            _thread.start()

    def get_task(self):
        while True:
            new_task = self.db_inst.get_new_task()
            if not new_task:
                time.sleep(30)
                continue

            _status = self.db_inst.change_task_status(new_task['uuid'], 'pending')
            self.main_queue.put(new_task)

    def run_consumer(self):

        self.start_threads()
        self.get_task()
        self.main_queue.join()


if __name__ == '__main__':
    queue_consumer = QueueTaskConsumer(db_file)
    queue_consumer.run_consumer()
